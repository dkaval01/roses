% Author - Domantas Kavaliauskas

% Customers
customer(hugh).
customer(ida).
customer(jeremy).
customer(stella).
customer(leroy).

% Rose variaties
rose(golden-sunset).
rose(cottage-beauty).
rose(pink-paradise).
rose(sweet-dreams).
rose(mountain-bloom).

% Events
event(wedding).
event(senior-prom).
event(thecharity-auction).
event(anniversary-party).
event(retirement-banquet).

% Additional items
item(place-cards).
item(streamers).
item(balloons).
item(gourmet-chocolates).
item(candles).

solve :- 
	% Rose variaty of each customer is different
	rose(HughRose), rose(IdaRose), rose(JeremyRose), rose(StellaRose), rose(LeroyRose),
	all_different([HughRose, IdaRose, JeremyRose, StellaRose, LeroyRose]),
	
	% Event of each customer is different
	event(HughEvent), event(IdaEvent), event(JeremyEvent), event(StellaEvent), event(LeroyEvent),
	all_different([HughEvent, IdaEvent, JeremyEvent, StellaEvent, LeroyEvent]),
	
	% Additional items each customer has bought is different
	item(HughItem), item(IdaItem), item(JeremyItem), item(StellaItem), item(LeroyItem),
	all_different([HughItem, IdaItem, JeremyItem, StellaItem, LeroyItem]),
	
	% A list of each customers rose, event and item
	Quadruples = [[hugh, HughRose, HughEvent, HughItem],
			[ida, IdaRose, IdaEvent, IdaItem],
			[jeremy, JeremyRose, JeremyEvent, JeremyItem],
			[stella, StellaRose, StellaEvent, StellaItem],
			[leroy, LeroyRose, LeroyEvent, LeroyItem]],
	% 1. Jeremy made a purchase for the senior prom.
	member([jeremy, _, senior-prom, _], Quadruples),
	
	% Stella (who didn't choose flowers for a wedding) picked the Cottage Beauty variety.
	\+ (StellaEvent = wedding),
	member([stella, cottage-beauty, _, _], Quadruples),
	
	% 2. Hugh (who selected the Pink Paradise blooms) didn't choose flowers 
	% for either thecharity auction or the wedding.
	member([hugh, pink-paradise, _, _], Quadruples),
	\+(HughEvent = wedding),
	\+(HughEvent = thecharity-auction),
			
	% 3. The customer who picked roses for an anniversary party also bought streamers.
	member([ _, _, anniversary-party, streamers], Quadruples),
	
	% The one shopping for a wedding chose the balloons.
	member([ _, _, wedding, balloons], Quadruples),
	
	% 4. The customer who bought the Sweet Dreams variety also bought gourmet chocolates.
	member([ _, sweet-dreams, _, gourmet-chocolates], Quadruples),
	
	% Jeremy didn't pick the Mountain Bloom variety.
	\+(JeremyRose = mountain-bloom),
	
	% 5. Leroy was shopping for the retirement banquet. 
	member([leroy, _, retirement-banquet, _], Quadruples),
	
	% The customer in charge of decorating the senior prom also bought the candles.
	member([ _, _, senior-prom, candles], Quadruples),
	
	tell(hugh, HughRose, HughEvent, HughItem),
	tell(ida, IdaRose, IdaEvent, IdaItem),
	tell(jeremy, JeremyRose, JeremyEvent, JeremyItem),
	tell(stella, StellaRose, StellaEvent, StellaItem),
	tell(leroy, LeroyRose, LeroyEvent, LeroyItem).
	
% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z, Q) :-
    write(X), write(' has bought '), write(Y), write(' roses for '),
    write(Z), write(' as well as '), write(Q), write('.'), nl.
